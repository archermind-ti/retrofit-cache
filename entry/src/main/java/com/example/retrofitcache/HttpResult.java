package com.example.retrofitcache;

/**
 * Created by Yale on 2017/6/13.
 */

public class HttpResult {
    private String message="";
    private boolean success = false;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message1) {
        this.message = message1;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success1) {
        this.success = success1;
    }
}
