package com.example.retrofitcache.rx1;

import java.util.concurrent.TimeUnit;

import com.example.retrofitcache.bean.GankOhos;
import ren.yale.ohos.retrofitcachelib.anno.Cache;
import ren.yale.ohos.retrofitcachelib.anno.Mock;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Yale on 2017/6/13.
 */

public interface ApiRx1 {


    @Cache(time = 5,timeUnit = TimeUnit.SECONDS)
    @GET("Android/9/1")
    Observable<GankOhos> getGankOhos(@Query("aa") String aa);

    @Mock(value = "{\"error\":false,\"results\":[{\"_id\":\"5941f5f3421aa92c7be61c16\",\"createdAt\":\"2017-06-15T10:50:27.317Z\",\"desc\":\"22222222\\\\u4effNice\\\\u9996\\\\u9875\\\\u56fe\\\\u7247\\\\u5217\\\\u88689\\\\u56fe\\\\u6837\\\\u5f0f\\\\uff0c\\\\u5e76\\\\u5b9e\\\\u73b0\\\\u62d6\\\\u62fd\\\\u6548\\\\u679c\",\"images\":[\"http://img.gank.io/4f54c011-e293-436a-ada1-dc03669ffb10\"],\"publishedAt\":\"2017-06-15T13:55:57.947Z\",\"source\":\"web\",\"type\":\"Ohos\",\"url\":\"http://www.jianshu.com/p/0ea96b952170\",\"used\":true,\"who\":\"www的事发生飞洒地方bbb\"}]}")
    @GET("Android/10/4")
    Observable<GankOhos> getRamMockGankOhos();

    @Mock(url = "https://gank.io/api/data/Android/10/2")
    @GET("Android/10/1")
    Observable<GankOhos> getUrlMockGankOhos();

    @Mock(assets = "resources/rawfile/mock.json")
    @GET("Android/10/5")
    Observable<GankOhos> getAssetsMockGankOhos();

}
