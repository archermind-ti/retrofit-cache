package com.example.retrofitcache.slice;

import com.alibaba.fastjson.JSON;
import com.example.retrofitcache.LogTestUtil;
import com.example.retrofitcache.ResourceTable;
import com.example.retrofitcache.bean.GankOhos;
import com.example.retrofitcache.rx1.OKHttpUtilsRx1;
import com.example.retrofitcache.rx2.OKHttpUtilsRx2;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.Subject;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;
import ren.yale.ohos.retrofitcachelib.RetrofitCache;
import rx.Subscriber;

public class MainAbilitySlice extends AbilitySlice {
    private Text mTextView;
    private Button rx1Test, rx1Ram, rx1Assets, rx1Url, rx2Test;
    private String result = "";
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        OKHttpUtilsRx1.INSTANCE.init(getApplicationContext());
        OKHttpUtilsRx2.INSTANCE.init(getApplicationContext());
        RetrofitCache.getInstance().init(this).enableMock(true);
        RetrofitCache.getInstance().addIgnoreParam("access_token");
        mTextView = (Text) findComponentById(ResourceTable.Id_tv_content);
        rx1Test = (Button) findComponentById(ResourceTable.Id_rx1_test);
        rx1Ram = (Button) findComponentById(ResourceTable.Id_rx1_ram);
        rx1Assets = (Button) findComponentById(ResourceTable.Id_rx1_assets);
        rx1Url = (Button) findComponentById(ResourceTable.Id_rx1_url);
        rx2Test = (Button) findComponentById(ResourceTable.Id_rx2_test);
        rx1Test.setClickedListener(this::onClick);
        rx1Ram.setClickedListener(this::onClick);
        rx1Assets.setClickedListener(this::onClick);
        rx1Url.setClickedListener(this::onClick);
        rx2Test.setClickedListener(this::onClick);

    }
    private EventHandler eventHandler = new EventHandler(EventRunner.getMainEventRunner()){
        @Override
        protected void processEvent(InnerEvent event) {
            super.processEvent(event);
            if (event.eventId == 0) {
                mTextView.setText(result);
            }

        }
    };



    private void testRx1(){
        OKHttpUtilsRx1.INSTANCE.getApi().getGankOhos("bb")
                .compose(OKHttpUtilsRx1.<GankOhos>ioMain())
                .subscribe(new Subscriber<GankOhos>() {
                    @Override
                    public void onStart() {
                        mTextView.setText("");
                    }

                    @Override
                    public void onCompleted() {
                    }
                    @Override
                    public void onError(Throwable e) {
                        LogTestUtil.d(e.toString());
                    }
                    @Override
                    public void onNext(GankOhos gankOhos) {
                        LogTestUtil.d(JSON.toJSONString(gankOhos));
                        result = JSON.toJSONString(gankOhos);
                        eventHandler.sendEvent(0);
                    }
                });
    }
    private void testRx1RamMock(){
        OKHttpUtilsRx1.INSTANCE.getApi().getRamMockGankOhos()
                .compose(OKHttpUtilsRx1.<GankOhos>ioMain())
                .subscribe(new Subscriber<GankOhos>() {
                    @Override
                    public void onStart() {
                        mTextView.setText("");
                    }

                    @Override
                    public void onCompleted() {
                    }
                    @Override
                    public void onError(Throwable e) {
                        LogTestUtil.d(e.toString());
                    }
                    @Override
                    public void onNext(GankOhos gankOhos) {
                        LogTestUtil.d(JSON.toJSONString(gankOhos));
                        result = JSON.toJSONString(gankOhos);
                        eventHandler.sendEvent(0);
                    }
                });
    }
    private void testRx1UrlMock(){
        OKHttpUtilsRx1.INSTANCE.getApi().getUrlMockGankOhos()
                .compose(OKHttpUtilsRx1.<GankOhos>ioMain())
                .subscribe(new Subscriber<GankOhos>() {
                    @Override
                    public void onStart() {
                        mTextView.setText("");
                    }

                    @Override
                    public void onCompleted() {
                    }
                    @Override
                    public void onError(Throwable e) {
                        LogTestUtil.d(e.toString());
                    }
                    @Override
                    public void onNext(GankOhos gankOhos) {
                        LogTestUtil.d(JSON.toJSONString(gankOhos));
                        result = JSON.toJSONString(gankOhos);
                        eventHandler.sendEvent(0);
                    }
                });
    }
    private void testRx2(){
        OKHttpUtilsRx2.INSTANCE.getApi().getGankOhos()
                .compose(OKHttpUtilsRx2.INSTANCE.<GankOhos>ioMain())
                .subscribe(new SubjectImpl<GankOhos>() {
                    @Override
                    public void onNext(GankOhos gankOhos) {
                        super.onNext(gankOhos);
                        mTextView.setText(JSON.toJSONString(gankOhos));
                    }
                });
    }




    private void testRx1AsssetslMock(){
        OKHttpUtilsRx1.INSTANCE.getApi().getAssetsMockGankOhos()
                .compose(OKHttpUtilsRx1.<GankOhos>ioMain())
                .subscribe(new Subscriber<GankOhos>() {
                    @Override
                    public void onStart() {
                        mTextView.setText("");
                    }

                    @Override
                    public void onCompleted() {
                    }
                    @Override
                    public void onError(Throwable e) {
                        LogTestUtil.d(e.toString());
                    }
                    @Override
                    public void onNext(GankOhos gankOhos) {
                        LogTestUtil.d(JSON.toJSONString(gankOhos));
                        result = JSON.toJSONString(gankOhos);
                        eventHandler.sendEvent(0);
                    }
                });
    }
    public void onClickRx1AssetsMock(){
        testRx1AsssetslMock();
    }
    public void onClickRx1(){

        testRx1();
    }
    public void onClickRx1RamMock(){
        testRx1RamMock();
    }
    public void onClickRx1UrlMock(){
        testRx1UrlMock();
    }
    public void onClickRx2(){
        mTextView.setText("");
        testRx2();
    }

    public void onClick(Component component) {
        switch (component.getId()){
            case ResourceTable.Id_rx1_test:
                onClickRx1();
                break;
            case ResourceTable.Id_rx1_ram:
                onClickRx1RamMock();
                break;
            case ResourceTable.Id_rx1_assets:
                onClickRx1AssetsMock();
                break;
            case ResourceTable.Id_rx1_url:
                onClickRx1UrlMock();
                break;
            case ResourceTable.Id_rx2_test:
                onClickRx2();
                break;
        }
    }

    private class SubjectImpl<GankOhos> extends Subject<GankOhos> {

        @Override
        public boolean hasObservers() {
            return false;
        }

        @Override
        public boolean hasThrowable() {
            return false;
        }

        @Override
        public boolean hasComplete() {
            return false;
        }

        @Override
        public Throwable getThrowable() {
            return null;
        }

        @Override
        protected void subscribeActual(Observer<? super GankOhos> observer) {

        }

        @Override
        public void onSubscribe(Disposable disposable) {

        }

        @Override
        public void onNext(GankOhos gankOhos) {

        }

        @Override
        public void onError(Throwable throwable) {

        }

        @Override
        public void onComplete() {

        }
    }
}
