package com.example.retrofitcache;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * Created by yale on 2017/11/29.
 */

public final class LogTestUtil {
    private LogTestUtil() {
    }

    static final HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x01, "retrofitcache");
    public static void d(String text){
        HiLog.debug(label,text);
    }
}
