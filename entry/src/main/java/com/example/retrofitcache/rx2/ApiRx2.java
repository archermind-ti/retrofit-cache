package com.example.retrofitcache.rx2;

import java.util.concurrent.TimeUnit;

import com.example.retrofitcache.bean.GankOhos;
import io.reactivex.Observable;
import ren.yale.ohos.retrofitcachelib.anno.Cache;
import ren.yale.ohos.retrofitcachelibrx2.anno.Mock;
import retrofit2.http.GET;

/**
 * Created by Yale on 2017/6/13.
 */

public interface ApiRx2 {

    @Mock(assets = "resources/rawfile/mock.json")
    @Cache(time = 10,timeUnit = TimeUnit.SECONDS)
    @GET("Android/9/1")
    Observable<GankOhos> getGankOhos();

}
