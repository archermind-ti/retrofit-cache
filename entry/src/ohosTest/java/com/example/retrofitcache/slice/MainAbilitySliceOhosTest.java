package com.example.retrofitcache.slice;

import com.alibaba.fastjson.JSON;
import com.example.retrofitcache.LogTestUtil;
import com.example.retrofitcache.bean.GankOhos;
import com.example.retrofitcache.rx1.OKHttpUtilsRx1;
import com.example.retrofitcache.rx2.OKHttpUtilsRx2;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.Subject;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import org.junit.Test;
import ren.yale.ohos.retrofitcachelib.RetrofitCache;
import rx.Subscriber;

public class MainAbilitySliceOhosTest {

    private final Context mContext = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
    @Test
    public void onStart() {
        OKHttpUtilsRx1.INSTANCE.init(mContext);
        OKHttpUtilsRx2.INSTANCE.init(mContext);
        RetrofitCache.getInstance().init(mContext).enableMock(true);
        RetrofitCache.getInstance().addIgnoreParam("access_token");
    }

    @Test
    public void onClickRx1AssetsMock() {
        OKHttpUtilsRx1.INSTANCE.getApi().getAssetsMockGankOhos()
                .compose(OKHttpUtilsRx1.<GankOhos>ioMain())
                .subscribe(new Subscriber<GankOhos>() {
                    @Override
                    public void onStart() {
                    }

                    @Override
                    public void onCompleted() {
                    }
                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }
                    @Override
                    public void onNext(GankOhos gankOhos) {
                        LogTestUtil.d(JSON.toJSONString(gankOhos));
                    }
                });
    }

    @Test
    public void onClickRx1() {
        OKHttpUtilsRx1.INSTANCE.getApi().getGankOhos("bb")
                .compose(OKHttpUtilsRx1.<GankOhos>ioMain())
                .subscribe(new Subscriber<GankOhos>() {
                    @Override
                    public void onStart() {
                    }

                    @Override
                    public void onCompleted() {
                    }
                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }
                    @Override
                    public void onNext(GankOhos gankOhos) {
                        LogTestUtil.d(JSON.toJSONString(gankOhos));
                    }
                });
    }

    @Test
    public void onClickRx1RamMock() {
        OKHttpUtilsRx1.INSTANCE.getApi().getRamMockGankOhos()
                .compose(OKHttpUtilsRx1.<GankOhos>ioMain())
                .subscribe(new Subscriber<GankOhos>() {
                    @Override
                    public void onStart() {

                    }

                    @Override
                    public void onCompleted() {
                    }
                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }
                    @Override
                    public void onNext(GankOhos gankOhos) {
                        LogTestUtil.d(JSON.toJSONString(gankOhos));
                    }
                });
    }

    @Test
    public void onClickRx1UrlMock() {
        OKHttpUtilsRx1.INSTANCE.getApi().getUrlMockGankOhos()
                .compose(OKHttpUtilsRx1.<GankOhos>ioMain())
                .subscribe(new Subscriber<GankOhos>() {
                    @Override
                    public void onStart() {

                    }

                    @Override
                    public void onCompleted() {
                    }
                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }
                    @Override
                    public void onNext(GankOhos gankOhos) {
                        LogTestUtil.d(JSON.toJSONString(gankOhos));
                    }
                });
    }

    @Test
    public void onClickRx2() {
        OKHttpUtilsRx2.INSTANCE.getApi().getGankOhos()
                .compose(OKHttpUtilsRx2.INSTANCE.<GankOhos>ioMain())
                .subscribe(new Subject<GankOhos>() {
                    @Override
                    public boolean hasObservers() {
                        return false;
                    }

                    @Override
                    public boolean hasThrowable() {
                        return false;
                    }

                    @Override
                    public boolean hasComplete() {
                        return false;
                    }

                    @Override
                    public Throwable getThrowable() {
                        return null;
                    }

                    @Override
                    protected void subscribeActual(Observer<? super GankOhos> observer) {

                    }

                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(GankOhos gankOhos) {
                        LogTestUtil.d(JSON.toJSONString(gankOhos));
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}