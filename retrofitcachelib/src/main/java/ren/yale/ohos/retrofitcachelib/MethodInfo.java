package ren.yale.ohos.retrofitcachelib;


import java.util.concurrent.TimeUnit;

/**
 * Created by yale on 2017/11/2.
 */

public class MethodInfo {

    private TimeUnit timeUnit;
    private int time;

    public TimeUnit getTimeUnit() {
        return timeUnit;
    }

    public void setTimeUnit(TimeUnit timeUnit1) {
        this.timeUnit = timeUnit1;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time1) {
        this.time = time1;
    }
}
