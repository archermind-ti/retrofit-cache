package ren.yale.ohos.retrofitcachelib.bean;


import java.util.concurrent.TimeUnit;

/**
 * Created by yale on 2018/1/23.
 */

public class CacheConfig {

    private TimeUnit timeUnit = TimeUnit.NANOSECONDS;
    private Long time = 0L;
    private boolean forceCacheNoNet = true;

    public TimeUnit getTimeUnit() {
        return timeUnit;
    }

    public void setTimeUnit(TimeUnit timeUnit1) {
        this.timeUnit = timeUnit1;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time1) {
        this.time = time1;
    }

    public boolean isForceCacheNoNet() {
        return forceCacheNoNet;
    }

    public void setForceCacheNoNet(boolean forceCacheNoNet1) {
        this.forceCacheNoNet = forceCacheNoNet1;
    }
}
