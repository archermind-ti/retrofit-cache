# RetrofitCache


#### 项目介绍

- 编程语言：Java 
- RetrofitCache让retrofit2+okhttp3+rxjava 配置缓存如此简单。通过注解配置，可以针对每一个接口灵活配置缓存策略；同时让每一个接口方便支持数据模拟，可以代码减小侵入性，模拟数据可以从内存，Assets，url轻松获取。
- 原项目Doc地址： https://github.com/yale8848/RetrofitCache

#### 安装教程

方式一

1. 下载工程retrofit-cache。
2. 启动 DevEco Studio，将retrofit-cache工程中的retrofitcachelibrx2模块或者retrofitcachelib模块导入到自己的工程中，两个模块分别对应的是rxjava2和rxjava1。
3. 在moudle级别下的build.gradle文件中添加依赖。
```
dependencies {
    implementation project(path: ':retrofitcachelib')  //retrofit2+okhttp3+rxjava1
	……
	implementation project(path: ':retrofitcachelibrx2')  //retrofit2+okhttp3+rxjava2
}
```

在sdk5，DevEco Studio2.1 beta3下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

方式二

```
在build.gradle文件中添加依赖：

dependencies {
	......
	implementation 'com.gitee.archermind-ti:retrofitcachelib:1.0.0-beta2'
	implementation 'com.gitee.archermind-ti:retrofitcachelibrx2:1.0.0-beta2'
    
}
```

#### 使用说明

##### 调用例子

- 不走缓存例子

```
@GET("users")
Observable<HttpResult> test();
```

- 缓存设置为20秒

 ```
@Cache(time = 20)
@GET("users")
Observable<HttpResult> test();
 ```

- 缓存设置为20分钟

 ```
@Cache(time = 20,timeUnit = TimeUnit.MINUTES)
@GET("users")
Observable<HttpResult> test();
 ```

- 默认时间缓存,默认是0秒

 ```
@Cache()
@GET("users")
Observable<HttpResult> test();
 ```

- 默认在无网的时候强制走缓存，forceCacheNoNet=false时无网络时不强制缓存

 ```
@Cache(forceCacheNoNet = false)
@GET("users")
Observable<HttpResult> test();
 ```

- 添加模拟数据（value,assets,url同时都配置的话，就按照这个顺序处理）

 ```
@Mock(value = "{\"data\":\"mockdata\"}") //模拟内存数据
@GET("users")
Observable<HttpResult> test();
 ```

 ```
@Mock(assets = "mock/mock.json") //从assets获取模拟数据
@GET("users")
Observable<HttpResult> test();
 ```

 ```
@Mock(url = "http://url.com/test") //从新的url请求数据
@GET("users")
Observable<HttpResult> test();
 ```

##### 调用方法

- 初始化

 ```
RetrofitCache.getInstance().init(this);
 ```

也可以修改默认配置，默认time=0，timeUnit = TimeUnit.SECONDS

```
RetrofitCache.getInstance().init(this).setDefaultTimeUnit(TimeUnit.MINUTES).setDefaultTime(1);
```

 - OkHttpClient初始化时配置缓存目录

 ```
okhttp3.OkHttpClient.Builder clientBuilder=new okhttp3.OkHttpClient.Builder();
...
int cacheSize = 200 * 1024 * 1024;
File cacheDirectory = new File(mContext.getCacheDir(), "httpcache");
Cache cache = new Cache(cacheDirectory, cacheSize);
OkHttpClient client =  clientBuilder.cache(cache).build();
...

 ```

- 给okhttp添加拦截器

 ```
okhttp3.OkHttpClient.Builder clientBuilder=new okhttp3.OkHttpClient.Builder();
...

//clientBuilder.addInterceptor(new MockInterceptor()); //如果只用Mock模拟功能,只需添加这个拦截器
clientBuilder.addInterceptor(new CacheForceInterceptorNoNet());
clientBuilder.addNetworkInterceptor(new CacheInterceptorOnNet());
...

 ```

 > 添加CacheForceInterceptorNoNet作用是在无网时强制走缓存,如果只添加了CacheInterceptorOnNet,那么在有网和无网的缓存策略就会一样


- 添加retrofit对象

```
Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .client(getOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
RetrofitCache.getInatance().addRetrofit(retrofit);
```
-  **添加 rx Observable compose**

```
api.test().compose(CacheTransformer.emptyTransformer())...

```

##### 进阶


- setCacheInterceptorListener 设置是否每一个接口都缓存

```
RetrofitCache.getInstance().setCacheInterceptorListener(
                new CacheInterceptorListener() {
            @Override
            public boolean canCache(Request request,Response response) {
                String res = "";
                try {
                    res = response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return true;
            }
});

```


- 设置是否走模拟数据,比如说在正式接口好了后可以如下设置，让模拟数据失效

```
RetrofitCache.getInstance().enableMock(false);
```

- 忽略某个参数;如果你对原url增加参数，可以设置忽略

```
 RetrofitCache.getInstance().addIgnoreParam("access_token");
```

#### 版本迭代


- v1.0.0


#### 版权和许可信息
- MIT License
