package ren.yale.ohos.retrofitcachelibrx2.util;


import ohos.app.Context;
import ohos.net.NetManager;

/**
 * Created by Yale on 2017/6/13.
 */

public final class NetUtils {
    private NetUtils() {
    }

    public static boolean isConnectNet(Context context){
        NetManager netManager = NetManager.getInstance(null);
        if (netManager != null && netManager.hasDefaultNet()) {
            return true;
        }
        return false;
    }
}
