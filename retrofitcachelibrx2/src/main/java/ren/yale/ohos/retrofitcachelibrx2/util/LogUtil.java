package ren.yale.ohos.retrofitcachelibrx2.util;


import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * Created by Yale on 2017/6/13.
 */

public final class LogUtil {
    private static final boolean DEBUG = false;

    private LogUtil() {
    }
    private static String format = "%{public}s";
    static final HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x01, "retrofitcache");

    public static void d(String text) {
        HiLog.debug(label, format, text);
    }

    public static void w(String text) {
        HiLog.warn(label, format, text);
    }

    public static void l(Exception e) {
        if (DEBUG) {
            HiLog.error(label,format, e.toString());
        } else {
            HiLog.warn(label, format, e.toString());
        }
    }
}
